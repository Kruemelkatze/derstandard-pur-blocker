# derStandard.at PUR Wall Blocker
Blocks the derstandard.at PUR paywall while keeping you logged in. **Prototype**, ergo buggy. Morally questionable. Useful in practice.

Does not prevent the PUR Wall from appearing, but changes your session id and automatically logs you in again.

![howto](howto.png)

