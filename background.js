
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete' && tab.active && tab.url && tab.url.startsWith("https://derstandard.at")) {

        chrome.cookies.getAll({ url: tab.url }, onCookiesFetched);

        function onCookiesFetched(cookieArray) {
            var cookies = mapCookies(cookieArray);
            if (!cookies.MGUID) {
                //If we do not have this, we can't do anything at all
                return;
            }

            let purWallHit = cookies.PurWallHit && +cookies.PurWallHit.value;
            let savedAuth = localStorage.getItem("Auth");
            let cookieAuth = cookies.Auth && cookies.Auth.value;
            let previousStage = localStorage.getItem("previousStage"); // Prevent reload loops if something breaks

            if (purWallHit && !cookieAuth && !savedAuth) {
                if (previousStage == "0") {
                    return;
                }
                // Can only change guid and PurWall
                let mguid = `GUID=${guid()}${cookies.MGUID.value.substring(41)}`;
                Promise.all([
                    setDerStandardCookie("MGUID", mguid, true),
                    setDerStandardCookie("PurWallHit", "0", true),
                ]).then(() => refreshPage(0));
            } else if (purWallHit && cookieAuth) {
                if (previousStage == "1") {
                    return;
                }
                // Stage 1: PurWallHit -> Store auth && change mguid
                localStorage.setItem("Auth", cookieAuth);
                let mguid = `GUID=${guid()}${cookies.MGUID.value.substring(41)}`;
                Promise.all([
                    setDerStandardCookie("MGUID", mguid, true),
                    setDerStandardCookie("PurWallHit", "0", true),
                ]).then(() => refreshPage(1));
            } else if (!purWallHit && !cookieAuth && savedAuth) {
                if (previousStage == "2") {
                    return;
                }
                // Stage 2: !PurwallHit & StoredAuth -> Restore Auth && delete it
                localStorage.removeItem("Auth");
                setDerStandardCookie("Auth", savedAuth, false).then(() => refreshPage(2));
            } else {
                localStorage.removeItem("previousStage");
            }
        }

        function mapCookies(cookies) {
            return cookies.reduce(function (map, obj) {
                map[obj.name] = obj;
                return map;
            }, {});
        }

        function setDerStandardCookie(name, value, dotSubDomain) {
            return new Promise((resolve, reject) => {
                let url = `https://derstandard.at`;
                let domain = `${dotSubDomain ? '.' : ''}derstandard.at`;
                chrome.cookies.set({ url, domain, name, value }, (r) => {
                    if (chrome.runtime.lastError) {
                        reject(chrome.runtime.lastError);
                    } else {
                        resolve(r);
                    }
                });
            })
        }

        function refreshPage(stage) {
            localStorage.setItem("previousStage", "" + stage);
            chrome.tabs.reload(tabId);
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }

    }
});



// chrome.webNavigation.onCompleted.addListener(function(details) {
//     chrome.tabs.executeScript(details.tabId, {
//         code: ' if (document.body.innerText.indexOf("Cat") !=-1) {' +
//               '     alert("Cat not found!");' +
//               ' }'
//     });
// });